define(['../engine/Mochi', './controls'], function(Mochi, Controls) {

    /**
     * Used to display a nametag on the characters
     */
    class NameTag extends Mochi.Component {

    }

    class CharacterFactory {
        constructor() {
            this.copAsset = new Mochi.Content.Asset("assets/Mochi_Cop.png");
            this.robAsset = new Mochi.Content.Asset("assets/Mochi_evil.png");
        }

        getAssets() {
            return [this.copAsset, this.robAsset];
        }

        makeCop(x, y, name, controls) {
            let cop = this._make(x, y, Character.Type.cop, this.copAsset, name, controls);
            cop.init();
            return cop;
        }

        makeRobber(x, y, name, controls) {
            let rob = this._make(x, y, Character.Type.rob, this.robAsset, name, controls);
            rob.init();
            return rob;
        }

        _make(x, y, type, asset, name, controls) {
            let char = new Character(x, y, type, name);
            char.add(new Mochi.Graphics.Sprite(asset, {width: 1, height: 1}));
            char.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Point()));
            // this adds the behavior for how one controls the character
            char.add(new Controls.Controller());
            // Add either a human or AI controller
            char.add(controls);
            return char;
        }
    }

    class Character extends Mochi.Entity {
        constructor(x, y, type, name) {
            super(x, y);
            this.charType = type;
            this.name = name;
        }
    }
    Character.Type = {
        cop: "cop",
        rob: "rob"
    };

    return {
        Character: Character,
        Factory: CharacterFactory
    };
});