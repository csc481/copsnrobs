// This grabs all the code that helps the game work
requirejs(['../engine/Mochi', './map', './mapselect', './countselect', 'playerselect', 'character', 'controls'],
function(Mochi, Map, MapSelect, CountSelect, PlayerSelect, Character, Controls) {

    /**
     * The main class that runs the game.
     */
    class CopsNRobsGame {
        constructor() {
            this.game = new Mochi.Game({
                width: window.innerWidth * .98,
                height: window.innerHeight * .98,
                fps: 60,
                startState: "load"
            });
            // the map files that will be displayed in the game.
            this.maps = new Mochi.Content.Asset("maps/maps.js");
            // game configuration
            this.config = {
                map: null,
                cops: 2,
                robs: 2,
                available: [],
                // number of circles of turns allowed until robber wins
                rounds: 3,
                // how far they can move on a turn
                moves: 4,
                character: null
            };

            // factories for producing game objects
            this.mapFactory = new Map.Factory();
            this.charFactory = new Character.Factory();

            // Load state that transitions to intro state when everything is loaded.
            this.game.addState(
                new Mochi.GameStates.LoadState("load", this.getAssets(), "intro")
            );
            // Set the current state to be the basic intro state
            this.game.addState(
                new Mochi.GameStates.IntroState("intro", ["Enter", " "], "mapSelect").setGameName("Cops & Robbers")
            );
            // add states for game configuration
            this.game.addState(
                new MapSelect("mapSelect", this.maps, "copSelect", this.config)
            );
            this.game.addState(
                new CountSelect("copSelect", "cops", "robSelect", this.config)
            );
            this.game.addState(
                new CountSelect("robSelect", "robs", "playerSelect", this.config)
            );
            this.game.addState(
                new PlayerSelect("playerSelect", "main", this.config)
            );
            // the main game state.
            this.game.addState(
                new Mochi.GameState("main").setup(this.update.bind(this), this.draw.bind(this), function() {}, this.enter.bind(this))
            );
            this.game.addState(
                new Mochi.GameStates.EndState("end", "intro", ["Enter", " "])
            );
        }

        /** Returns all assets needed to be loaded by the game */
        getAssets() {
            return [this.maps].concat(this.mapFactory.getAssets()).concat(this.charFactory.getAssets());
        }

        start() {
            this.game.start();
        };

        /**
         * Called when the main game state is entered.
         */
        enter(game, states, lastState) {
            // the map that we are going to play
            console.log("Spawning new game for config: ", this.config);
            // set up event system
            this.events = new Mochi.Events.EventBus();
            // set up event bindings
            this.events.subscribe("robber_win", this.robberWin.bind(this));

            // set up our map
            this.scene = this.mapFactory.make(this.config.map.data, this.game.getPixelWidth(), this.game.getPixelHeight());

            // check for collisions for major game changes
            this.scene.managers.collision.events.subscribe("collision", this.onCollision.bind(this));

            // initialize rounds
            this.roundsLeft = this.config.rounds;
            // first person has the current turn
            this.curTurn = -1;
            // create our cops and robbers, will always add robbers first (they must go first).
            this.characters = [];
            for (let i = 0; i < this.config.available.length; i++) {
                let spot = this.scene.getRandomOpenSpot();
                let entity = null;
                // Only give AI if not human
                if (this.config.available[i] === this.config.character)
                    var controls = new Controls.Human(this.game.input);
                else
                    var controls = new Controls.Ai();
                if (this.config.available[i].type === "cop")
                    entity = this.charFactory.makeCop(spot.x, spot.y, this.config.available[i].text, controls);
                else
                    entity = this.charFactory.makeRobber(spot.x, spot.y, this.config.available[i].text, controls);
                this.scene.addEntity(entity);
                this.characters.push(entity);
            }

            // create UI elements to display who's turn it is
            this.curTurnUi = new Mochi.Graphics.Ui.Elements.Text("Initializing board...");

            // prep turn order
            this.changeTurn(0);
        }

        /**
         * Renders the game
         * @param ctx The context needed.
         */
        draw(ctx) {
            ctx.save();
            // TODO integrate into draw
            this.scene.managers.unitDraw.clear(ctx, this.game);
            this.scene.draw(ctx);
            this.curTurnUi.draw(ctx, this.game.getPixelWidth() / 2, this.curTurnUi.getHeight());
            ctx.restore();
        }

        /** The update loop */
        update(ms) {
            this.scene.update(ms);
        }

        /**
         * Changes the turn of the game to be the given character.
         * @param charIndex The index in characters.
         */
        changeTurn(charIndex) {
            let lastTurn = this.curTurn;
            // unbind on the last character and clear the selections made
            if (lastTurn !== -1) {
                this.characters[lastTurn].events.unsubscribe("turn_end", this.endTurnId);
                this.scene.events.broadcast("clearSelections");
            }
            this.curTurn = charIndex;
            let curRound = this.config.rounds - this.roundsLeft;
            this.curTurnUi.setText("Round " + curRound + "/" + this.config.rounds + ", Current Turn: " + this.characters[charIndex].name);
            // we subscribe to the events of the entity who currently has the turn
            this.endTurnId = this.characters[charIndex].events.subscribe("turn_end", this.nextTurn.bind(this));
            // we tell the entity that it is their turn and how many moves they have this turn
            this.characters[charIndex].events.broadcast("turn_begin", {
                map: this.scene,
                moves: this.config.moves,
                characters: this.characters
            });
        }

        /** Goes to the next person in the list, will reset rounds. */
        nextTurn() {
            let next = this.curTurn + 1;
            if (next >= this.characters.length) {
                next = 0;
                this.roundsLeft--;
                if (this.roundsLeft < 0)
                    this.events.broadcast("robber_win");
            }
            this.changeTurn(next);
        }

        onCollision(data) {
            // if either is a cop or a robber, the robber dies
            let dead = null;
            if (data.victim.charType === 'rob' && data.culprit.charType === 'cop')
                dead = data.victim;
            if (data.culprit.charType === 'rob' && data.victim.charType === 'cop')
                dead = data.culprit;
            if (dead === null)
                return;
            // we need to kill the robber
            this.removeRobber(dead);
            // if we have no robbers, cops win.
            if (this._hasNoRobbers(this.characters))
                this.copWin();
        }

        removeRobber(rob) {
            console.log("Killing robber", rob);
            let index = this.characters.indexOf(rob);
            this.characters.splice(rob, 1);
            this.scene.removeEntity(rob);
            console.log(this.characters);
            // if they were before us, fix our index
            if (index < this.curTurn) {
                this.curTurn--;
            }
        }

        _hasNoRobbers(list) {
            for (let i = 0; i < list.length; i++) {
                console.log(list[i].charType);
                if (list[i].charType === 'rob')
                    return false;
            }
            return true;
        }

        /**
         * Called when robbers win
         */
        robberWin() {
            console.log("Robbers win!");
            this.game.swapState("end");
        }

        copWin() {
            console.log("Cops win!");
            this.game.swapState("end");
        }

    }

    var copsNRobs = new CopsNRobsGame();
    // Initialize and begin the game.
    copsNRobs.start();
    console.log(copsNRobs);
});

