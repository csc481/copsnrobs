define(['../engine/Mochi'], function(Mochi) {

    class PlayerSelect extends Mochi.GameState {

        constructor(name, next, config) {
            super(name);
            this.next = next;
            this.config = config;
        }

        init (game, states) {
            // Keep reference to the game for width/height and state swaping.
            this.game = game;
            // create the list based on our current config
            this.config.available = this._createAvailable(this.config.cops, this.config.robs);
            // create a select for the maps
            this.mapSelector = new Mochi.Graphics.Ui.Elements.Select(this.config.available);
            this.mapSelector.bindNavigationKeys(game.input, ['w'], ['s'], ['Enter']);
            // when a map is selected, call select.
            this.mapSelector.events.subscribe("selected", this.select.bind(this));
        }

        /** Whenever this state is entered, it attempts to load all assets */
        enter (game, states, lastState) {
            // create background
            this.background = new Mochi.Graphics.Ui.Elements.Background();
            this.header = new Mochi.Graphics.Ui.Elements.Text("Select your character (w/s/enter):").setStyle(new Mochi.Graphics.Ui.Styles.Default.Heading());
        }

        _createAvailable(cops, robs) {
            let available = [];
            for (let i = 0; i < robs; i++) {
                available.push({
                    type: "rob",
                    text: "Robber " + i
                });
            }
            for (let i = 0; i < cops; i++) {
                available.push({
                    type: "cop",
                    text: "Cop " + i
                });
            }
            return available;
        }

        select(character) {
            console.log("Character selected: ", character);
            this.config.character = character;
            this.game.swapState(this.next);
        }

        update(ms) {}

        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            this.background.draw(ctx, 0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            this.header.draw(ctx, this.game.getPixelWidth() / 2, this.header.getHeight());
            this.mapSelector.draw(ctx, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2 - this.mapSelector.getHeight() / 2);
            ctx.restore();
        }
    }

    return PlayerSelect;
});