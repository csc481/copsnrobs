define(['../engine/Mochi'], function(Mochi) {

    class CountSelect extends Mochi.GameState {

        constructor(name, type, next, config) {
            super(name);
            this.type = type;
            this.next = next;
            this.config = config;
        }

        init (game, states) {
            // Keep reference to the game for width/height and state swaping.
            this.game = game;
            // create a select for the maps
            this.mapSelector = new Mochi.Graphics.Ui.Elements.Counter(1, 4, 2);
            this.mapSelector.bindNavigationKeys(game.input, ['w'], ['s'], ['Enter']);
            // when a map is selected, call select.
            this.mapSelector.events.subscribe("selected", this.select.bind(this));
        }

        /** Whenever this state is entered, it attempts to load all assets */
        enter (game, states, lastState) {
            // create background
            this.background = new Mochi.Graphics.Ui.Elements.Background();
            this.header = new Mochi.Graphics.Ui.Elements.Text("How many " + this.type + " (w/s/enter):").setStyle(new Mochi.Graphics.Ui.Styles.Default.Heading());
        }

        select(count) {
            console.log(this.type + " selected: ", count);
            this.config[this.type] = count;
            this.game.swapState(this.next);
        }

        update(ms) {}

        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            this.background.draw(ctx, 0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            this.header.draw(ctx, this.game.getPixelWidth() / 2, this.header.getHeight());
            this.mapSelector.draw(ctx, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2 - this.mapSelector.getHeight() / 2);
            ctx.restore();
        }
    }

    return CountSelect;
});