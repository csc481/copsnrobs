define(['../engine/Mochi', './character'], function(Mochi, Character) {

    /**
     * Used to produce maps.
     */
    class MapFactory {
        constructor() {
            this.tilePath = new Mochi.Content.Asset("assets/TempPath.png");
            this.tileWall = new Mochi.Content.Asset("assets/TempWall.png");
            this.tileSelect = new Mochi.Content.Asset("assets/Path_Selected.png");
        }

        getAssets() {
            return [this.tilePath, this.tileWall, this.tileSelect];
        }

        make(mapData, width, height) {
            let map = new Map(mapData, width, height, {
                path: this.tilePath,
                wall: this.tileWall,
                select: this.tileSelect
            });
            map.init();
            return map;
        }
    }

    /**
     * Used to maintain information about the map of the current game.
     */
    class Map extends Mochi.Scene {
        constructor(data, width, height, tiles) {
            super();
            let rows = data.map.length;
            let cols = data.map[0].length;
            this.map = data.map;
            // initialize the map tiles as sprites
            this.tiles = {
                path: new Mochi.Graphics.Sprite(tiles.path, { height: 1, width: 1 }),
                select: new Mochi.Graphics.Sprite(tiles.select, { height: 1, width: 1 }),
                wall: new Mochi.Graphics.Sprite(tiles.wall, { height: 1, width: 1 }),
            };
            // add all the managers for this scene
            this.us = new Mochi.Util.UnitSystem2d(rows, cols, width, height);
            this.addManager(new Mochi.Physics.CollisionManager(0, 0, cols, rows, 1, 1), "collision");
            this.addManager(new Mochi.Graphics.UnitDrawManager(this.us), "unitDraw");

            // add selection ability
            this.selected = [];
            this.events.subscribe("selectTile", this.selectTile.bind(this));
            this.events.subscribe("clearSelections", this.clearSelections.bind(this));
        }

        selectTile(coords) {
            // TODO check if we need multiple selection
            this.selected = [
                coords
            ];
        }

        clearSelections() {
            this.selected = [];
        }

        isSelected(x, y) {
            for (let i = 0; i < this.selected.length; i++) {
                if (this.selected[i].x === x && this.selected[i].y === y)
                    return true;
            }
            return false;
        }

        draw (ctx) {
            // draw the grid
            this._drawMap(ctx);
            // draw all our entities
            super.draw(ctx);
        }

        _drawMap(ctx) {
            for (var i = 0; i < this.us.rows; i++) {
                for (var j = 0; j < this.us.cols; j++) {
                    let type = this._getTileType(j, i);
                    // swap the appearance if we are selected
                    if (this.isSelected(j, i))
                        type = "select";
                    let x = this.us.unitsToPixelsX(j);
                    let y = this.us.unitsToPixelsY(i);
                    let width = this.us.unitsToPixelsX(this.tiles[type].width);
                    let height = this.us.unitsToPixelsY(this.tiles[type].height);
                    this.tiles[type].draw(ctx, x, y, width, height);
                }
            }
        }

        _getTileType(x, y) {
            let type = this.map[y][x];
            if (type === 0)
                return "path";
            return "wall";
        }

        _getEntityAt(x, y) {
            for (let e = 0; e < this.entities.length; e++) {
                if (this.entities[e].x === x && this.entities[e].y === y)
                    return this.entities[e];
            }
            return null;
        }

        /**
         * asks if the provided tile (row, col) is open, which means it neither has a cop/robber or wall
         * @param x The x position to search
         * @param y The y position to search
         */
        isOpen(x, y) {
            return this._getTileType(x, y) === "path" && this._getEntityAt(x, y) === null;
        }

        /**
         * Returns true if a cop can move to the given location.
         * @param x
         * @param y
         */
        canCopMove(x, y) {
            if (this._getTileType(x, y) !== "path")
                return false;
            return true;
            /*let entity = this._getEntityAt(x, y);
            // if there is an entity and it is a cop, we can't move there.
            return !(entity !== null && entity.charType === Character.Character.Type.cop);*/

        }

        /**
         * Returns true if a robber can move to the given location.
         * @param x
         * @param y
         */
        canRobMove(x, y) {
            if (this._getTileType(x, y) !== "path")
                return false;
            let entity = this._getEntityAt(x, y);
            // if there is an entity and it is a cop, we can't move there.
            return !(entity !== null && entity.charType === Character.Character.Type.cop);
        }

        /**
         * @return object random open x, y coordinate.
         */
        getRandomOpenSpot() {
            let randX = Math.floor(Math.random() * this.us.cols);
            let randY = Math.floor(Math.random() * this.us.rows);
            if (this.isOpen(randX, randY))
                return {x: randX, y: randY};
            // recursively try and get another
            return this.getRandomOpenSpot();
        }
    }

    return {
        Map: Map,
        Factory: MapFactory
    };
});