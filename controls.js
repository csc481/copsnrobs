define(['../engine/Mochi'], function(Mochi) {

    /**
     * Gives actions to a character that they can do within a turn.
     */
    class Controller extends Mochi.Component {
        constructor() {
            super();
            this.movesLeft = 0;
        }

        init(entity) {
            this.entity = entity;
            this.entity.events.subscribe("turn_begin", this.onNewTurn.bind(this));
            this.entity.events.subscribe("try_move", this.move.bind(this));
        }

        /**
         * Invoked when they receive a new turn.
         */
        onNewTurn(data) {
            // reset our moves
            this.movesLeft = data.moves;
            this.map = data.map;
        }

        move(coords) {
            let x = coords.x;
            let y = coords.y;
            // only move if they have moves left
            if (this.movesLeft <= 0)
                return;
            // make sure it is a valid move
            if (!this.canMove(x, y))
                return;
            // if the move is > 1 tile we cannot allow it
            if ((Math.abs(this.entity.x - x) + Math.abs(this.entity.y - y)) !== 1)
                return;
            // execute move
            this.entity.x = x;
            this.entity.y = y;
            this.movesLeft--;
            // broadcast that we moved
            console.log("Character moved", this.entity.name);
            this.entity.events.broadcast("turn_move", {x: x, y: y, entity: this.entity, movesLeft: this.movesLeft});
            // if out of moves, broadcast turn_end.
            if (this.movesLeft === 0)
                this.entity.events.broadcast("turn_end");
        }

        hasMovesLeft() {
            return this.movesLeft > 0;
        }

        canMove(x, y) {
            if (this.entity.charType === "cop")
                return this.map.canCopMove(x, y);
            else if (this.entity.charType === "rob")
                return this.map.canRobMove(x, y);
            // what is this entity??
            return false;
        }
    }
    Controller.Name = "controller";

    /**
     * This component is used to implement how the controller is modified (via a human).
     * Requires a controller.
     */
    class HumanControls extends Mochi.Component {
        /**
         * Creates a human controller for an entity. Requires an InputManager
         * for bindings.
         * @param input The Input object.
         */
        constructor(input) {
            super();
            this.input = input;
        }

        /**
         * Invoked when the component has been attached to an entity.
         * @param entity The entity that this component is attached to
         */
        init(entity) {
            this.entity = entity;
            // store the controller that we will use
            this.controller = this.entity.controller;
            this.entity.events.subscribe("turn_begin", this.onNewTurn.bind(this));
            this.input.onKeyDown("w", this.selectUp.bind(this));
            this.input.onKeyDown("s", this.selectDown.bind(this));
            this.input.onKeyDown("a", this.selectLeft.bind(this));
            this.input.onKeyDown("d", this.selectRight.bind(this));
            this.input.onKeyDown("Enter", this.end.bind(this));
            this.input.onKeyDown(" ", this.move.bind(this));
        }

        /**
         * Should set up the bindings
         */
        onNewTurn(data) {
            this.map = data.map;
            this.next = null;
        }

        /** tells the character to move */
        move() {
            if (!this.controller.hasMovesLeft())
                return;
            if (!this.next)
                return;
            // ask the controller to move
            this.entity.events.broadcast("try_move", this.next);
        }

        end() {
            this.entity.events.broadcast("turn_end");
        }

        selectUp() {
            this._select(this.entity.x, this.entity.y - 1);
        }
        selectDown() {
            this._select(this.entity.x, this.entity.y + 1);
        }
        selectLeft() {
            this._select(this.entity.x - 1, this.entity.y);
        }
        selectRight() {
            this._select(this.entity.x + 1, this.entity.y);
        }

        _select(x, y) {
            if (!this.controller.hasMovesLeft())
                return;
            this.map.events.broadcast("selectTile", {
                x: x,
                y: y
            });
            this.next = {x: x, y: y};
        }

    }
    HumanControls.Name = "humanControls";

    /**
     * Uses A* to calculate fastest path
     */
    class AiControls extends Mochi.Component {
        constructor() {
            super();
        }

        init(entity) {
            this.entity = entity;
            // store the controller that we will use
            this.controller = this.entity.controller;
            this.entity.events.subscribe("turn_begin", this.onNewTurn.bind(this));
        }

        /**
         * Should set up the bindings
         */
        onNewTurn(data) {
            this.map = data.map;
            if (this.entity.charType === "cop")
                this.makeCopMove(data.characters);
            else if (this.entity.charType === "rob")
                this.makeRobMove();
        }

        /**
         * Sends out a series of try_move events for coordinates for a cop move.
         * @param characters An array of all the objects in the map
         */
        makeCopMove(characters) {
            let map = [];
            for (let i = 0; i < this.map.map.length; i++) {
                map.push([]);
                for (let j = 0; j < this.map.map[0].length; j++) {
                    map[i].push(this.map.map[j][i]);
                }
            }
            // use this.controller.movesLeft to see how many moves
            // characters contains data about all the people on the board
            // call try_move on this.events.broadcast and pass coordinate to move to
            while (this.controller.hasMovesLeft()) {
                let path = null;
                for (let i = 0; i < characters.length; i++) {
                    if (characters[i].charType === "rob") {
                        let thisPath = Mochi.Ai.PathFinding.aStar(map, [this.entity.x, this.entity.y], [characters[i].x, characters[i].y]);
                        if (thisPath === []) {
                            continue;
                        }
                        if (path === null || thisPath.length < path.length) {
                            path = thisPath;
                        }
                    }
                }
                let coords;
                if (path.length < this.controller.movesLeft) {
                    coords = path[path.length - 1];
                } else {
                    coords = path[this.controller.movesLeft];
                }
                if (path[1] == null) {
                    this.controller.movesLeft = 0;
                    this.entity.events.broadcast("turn_end");
                    return;
                }
                coords = path[1];
                this.entity.events.broadcast("try_move", {x: coords[0], y: coords[1]});
            }
        }

        /**
         * Sends out a series of try_move events for coordinates of a rob move.
         */
        makeRobMove(characters) {

            while (this.controller.hasMovesLeft()) {
                let xy = Math.random() > .5;
                let posNeg = Math.random() > .5;
                let x = this.entity.x;
                let y = this.entity.y;
                if (xy) {
                    if (posNeg) {
                        x += 1;
                    } else {
                        x -= 1;
                    }
                } else {
                    if (posNeg) {
                        y += 1;
                    } else {
                        y -= 1;
                    }
                }
                this.entity.events.broadcast("try_move", {x:x , y:y });
            }

        }

    }
    AiControls.Name = "aiControls";

    return {
        Controller: Controller,
        Human: HumanControls,
        Ai: AiControls
    };
});