# Team:
 1. Kenny Jones (kgjones3)
 2. Justin Patzer (jepatzer)
 3. Robby Seligson (rfseligs)

# For the Professor:

Everything is packaged inside. Just open up index.html, which uses requirejs (see libraries in Engine readme) to
load main.js.

# Controls

Use wasd to select where to move, then press space to confirm the move.

Press enter to end your turn before you are out of movement.

AI uses a*.

