let maps = [
    {
        text: "Basic",
        file: "maps/map1.js"
    },
    {
        text: "The Face",
        file: "maps/map2.js"
    },
    {
        text: "Bow & Arrow",
        file: "maps/map3.js"
    },
	{
        text: "Prison",
		file: "maps/map4.js"
		
	},
	{
        text: "Symmetry",
		file: "maps/map5.js"
		
	}
];

define([], function() {
    return maps;
});